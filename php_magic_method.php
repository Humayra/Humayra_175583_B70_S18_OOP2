<?php


class Person{

    public $myProperty="A1", $myP1="A2", $myP2="A3", $myP3="A4";

    public function __sleep()
    {
        // TODO: Implement __sleep() method.
        echo "I'm inside ".__METHOD__."<br>";

        return array('myP1','myP3');
    }

    public function __wakeup()
    {
        // TODO: Implement __wakeup() method.
        echo "I'm inside ".__METHOD__."<br>";
        echo "DB connection again<br>";
    }

    public static function __callStatic($name, $arguments)
    {
        // TODO: Implement __callStatic() method.
        echo "I'm inside ".__METHOD__."<br>";
        echo $name."<br><br>";
        echo "<pre>";
        var_dump($arguments);
        echo "</pre>";
    }

    public static function doSomethingStatic(){

        echo "I'm inside ".__METHOD__."<br>";
    }

    public function __call($name, $arguments)
    {
        // TODO: Implement __call() method.
        echo "I'm inside ".__METHOD__."<br>";
        echo $name."<br><br>";
        echo "<pre>";
        var_dump($arguments);
        echo "</pre>";
    }

    public function __isset($name)
    {
        // TODO: Implement __isset() method.
        echo "I'm inside ".__METHOD__."<br>";
        echo $name."<br><br>";
    }

    public function __unset($name)
    {
        // TODO: Implement __unset() method.
        echo "I'm inside ".__METHOD__."<br>";
        echo $name."<br><br>";
    }

    public function __set($name, $value)
    {
        // TODO: Implement __set() method.
        echo "I'm inside ".__METHOD__."<br>";
        echo $name." ".$value."<br><br>"; //here, aPropertyDoesNotExist assigns to name & Helo World assigns to value
    }
    public function __get($name)
    {
        // TODO: Implement __get() method.
        echo "I'm inside ".__METHOD__."<br>";
        echo $name."<br><br>";
    }

    public function __construct()
    {
        //task for db connection
        echo "Database Connection Successful<br><br>";
    }

    public function __destruct()
    {
        // TODO: Implement __destruct() method.
        echo "Maff kore diyen...<br><br>";
    }

    public function doSomething(){

        echo "I'm doing something from ".__METHOD__."<br><br>";
    }
}

    $objPerson1 = new Person();
    $objPerson2 = new Person();
    $objPerson3 = new Person();

    unset($objPerson2); //__destruct is called here

    $objPerson1->doSomething(); //doSomething function is called here

    $objPerson1->aPropertyThatDoesNotExist = "Hello World";  //__set is called here

    echo $objPerson1->aPropertyThatDoesNotExist; //__get is called here

    if(isset($objPerson1->aPropertyThatDoesNotExist)){  //__isset is called here

    }
        else{

        }

    unset($objPerson1->aPropertyThatDoesNotExist); //__unset is called here

    $objPerson1->doingSomething("Hello World", 123, true, 12.3);  //__call is called here

    Person::doSomethingStatic(); //static function doSomething is called here

    Person::doSomethingDoesNotStatic(); //__callStatic is called here
    Person::doSomethingDoesNotStatic(123);

    $myVar = serialize($objPerson1); //__sleep is called here
    var_dump($myVar);

    $myPerson1Object = unserialize($myVar); //__wakeup is called here
    var_dump($myPerson1Object);

    echo "<hr>";

    // __set magic method


